# $Id: top.rb,v 1.2 2002/04/15 02:36:21 tommy Exp $

def top(ml)

  h = ML::new(ml)
  minno = h.min > 0 ? (h.min-1)/100*100+1 : 0
  maxno = h.max
  path = ENV['SCRIPT_NAME'] + "/#{ml}"

  puts <<EOS
Content-Type: text/html; charset=#{$Charset}

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=#{$Charset}">
<meta http-equiv="Content-Style-Type" content="text/css">
<title>#{ml}:ML</title>
<link rel=stylesheet type="text/css" href="#{$CSS_URL}/w3ml.css">
</head>
<body>
<h1>#{ml}</h1>
EOS

  puts <<EOS
<h2>#{$SearchTitle}</h2>
<form method="get" action="#{path}/search">
<input name="body" type="text"><input type="submit" value="#{$SearchTitle}">
<a href="#{path}/search">#{$AdvancedSearch}</a>
</form>

EOS

  puts "<h2>#{$ListTitle}</h2>"
  puts '<table border>'
  if minno > 0 then
    minno.step(maxno, $Step*5) do |n|
      puts '<tr>'
      n.step([n+$Step*5-1, maxno].min, $Step) do |m|
	to = m+$Step-1
	puts "<td><a href=\"#{path}/list/#{m}-#{to}\">#{m}-#{to}</a></td>"
      end
      puts '</tr>'
    end
  end
  puts '</table>'

  puts "<h2>#{$ThreadTitle}</h2>"
  puts '<table border>'
  if minno > 0 then
    minno.step(maxno, $Step*5) do |n|
      puts '<tr>'
      n.step([n+$Step*5-1, maxno].min, $Step) do |m|
	to = m+$Step-1
	puts "<td><a href=\"#{path}/thread/#{m}-#{to}\">#{m}-#{to}</a></td>"
      end
      puts '</tr>'
    end
  end
  puts '</table>'

  puts <<EOS
<hr>
<p>
Powered by <a href="http://www.tmtm.org/ruby/w3ml/">w3ml</a>
</p>
</body>
</html>
EOS

end
