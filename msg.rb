$:.unshift( File::dirname( __FILE__ ) )
require 'cgi'
require 'util'

if $Charset.downcase == 'euc-jp' then
  begin
    require 'uconv'
    def filename_convert(s)
      Uconv::euctou8 s
    end
  rescue LoadError
    def filename_convert(s) s end
  end
else
  def filename_convert(s) s end
end

def to_html_body(str)
  s = CGI.escapeHTML(str)
  s.gsub(/^((&gt; *|\| *)+).*$/) do |l|
    n = $1.scan(/&gt;|\|/).length
    if n > 5 then n = 5 end
    "<span class=\"quote#{n}\">#{l}</span>"
  end
end

def add_tag(str, ml)
  str = str.gsub(%r((ftp|https?)://([/0-9a-zA-Z.:_=?#%~+-]|&amp;)+)) {%(<a href="#{$&}">#{$&}</a>)}
  str.gsub(/\[#{Regexp::quote ml}.(\d+)\]/) do |l|
    "<a href=\"#{$1}\">#{l}</a>"
  end
end

def msg(ml, arg)
  n = arg.to_i
  if n <= 0 then
    not_found
  end

  h = ML::new(ml)
  if not h.has? n then
    not_found
  end

  m = h[n]
  mpath = ENV['SCRIPT_NAME'] + "/#{ml}"

  header = CGI.escapeHTML(m.header).gsub(/\n/, "<br>\n").
    sub(/\A(.*\&lt;)(.*)(\&gt;)/){"#{$1}<a href=\"mailto:#{$2}\">#{$2}</a>#{$3}"}
  header = hide_address(CGI.escapeHTML(m.header).gsub(/\n/, "<br>\n"))

  prev_html = m.prev ? "<a href=\"#{mpath}/msg/#{m.prev}\">#{$PrevHTML}</a>" : $PrevHTML
  next_html = m.next ? "<a href=\"#{mpath}/msg/#{m.next}\">#{$NextHTML}</a>" : $NextHTML
  s = (n-1)/$Step*$Step+1
  e = s + $Step - 1
  buttons = "#{prev_html}#{next_html}<a href=\"#{mpath}/list/#{s}-#{e}\">#{$ListHTML}</a><a href=\"#{mpath}/thread/#{s}-#{e}\">#{$ThreadHTML}</a>"
  buttons << "<a href=\"#{mpath}/raw/#{n}\">#{$RawHTML}</a>" if $StoreRaw && $GetRaw

  puts <<EOS
Content-Type: text/html; charset=#{$Charset}

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=#{$Charset}">
<meta http-equiv="Content-Style-Type" content="text/css">
<title>#{ml}:#{n}</title>
<link rel=stylesheet type="text/css" href="#{$CSS_URL}/w3ml.css">
</head>
<body>
<p class="button">#{buttons}</p>
<p class="id">
#{ml}:#{n}
</p>
<div class="header">
<p>
#{header}
</p>
</div>
<div class="body">
<pre>
#{add_tag to_html_body(hide_address(m.body)), ml}
</pre>
</div>
EOS

  if m.have_attach? then
    puts <<EOS
<div class="attach">
<h3 class="attachtitle">#{$AttachTitle}</h3>
<ul>
EOS
    m.attach_list.each do |path, fname, ct|
      puts <<EOS
<li><a href="#{mpath}/msg/#{n}/#{path}/#{CGI.escape(filename_convert(fname || ''))}">#{CGI.escapeHTML(fname || '<noname>')} [#{CGI.escapeHTML(ct)}]</a>
EOS
    end
    puts <<EOS
</ul>
</div>
EOS
  end

  puts "<p class=\"button\">#{buttons}</p>"

  if m.reply_to > 0 or m.followed.length > 0 then
    pa = m.no
    while h.has?(pa) and h[pa].reply_to > 0
      pa = h[pa].reply_to
    end
    require LIB_DIR+'tree'
    puts '<pre class="list">'
    print_tree(mpath, h[pa], nil, '', '', m.no)
    puts '</pre>'
  end

  puts <<EOS
</body>
</html>
EOS

end
