# $Id: list_ml.rb,v 1.3 2002/04/15 04:16:41 tommy Exp $

def list_ml()
  path = ENV['SCRIPT_NAME']
  puts <<EOS
Content-Type: text/html; charset=#{$Charset}

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=#{$Charset}">
<meta http-equiv="Content-Style-Type" content="text/css">
<title>W3ml</title>
<link rel=stylesheet type="text/css" href="#{$CSS_URL}/w3ml.css">
</head>
<body>
<h1><a href="http://www.tmtm.org/ruby/w3ml/">W3ml</a></h1>
<ul>
EOS
  ml_list.sort.each do |m|
    puts %(<li><a href="#{path}/#{m}/">#{m}</a>)
  end
  puts <<EOS
</ul>
</body>
</html>
EOS
exit
end
