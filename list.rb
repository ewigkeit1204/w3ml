$:.unshift( File::dirname( __FILE__ ) )
require 'cgi'
require 'util'

def list(ml, arg)
  not_found if arg == nil
  path = ENV['SCRIPT_NAME'] + "/#{ml}"
  h, s, e = put_head(ml, 'list', arg)
  h.range(s, e) do |n|
    printf "<a href=\"%s\">%s %5d %s [%-20.20s] %-40s</a>\n",
      "#{path}/msg/#{n}",
      h[n].have_attach? ? $WithAttachHTML[0] : $WithAttachHTML[1],
      n,
      h[n].time.strftime('%Y-%m-%d %H:%M'),
      CGI.escapeHTML(hide_address(h[n].from)),
      CGI.escapeHTML(h[n].subject)
  end
  put_tail
end

def thread(ml, arg)
  not_found if arg == nil
  path = ENV['SCRIPT_NAME'] + "/#{ml}"
  require LIB_DIR+'tree'
  h, s, e = put_head(ml, 'thread', arg)
  @deleted = {}
  h.range(s, e) do |n|
    next if @deleted.has_key? n
    next if h[n].reply_to >= s
    print_tree(path, h[n], @deleted)
  end
  put_tail
end

def put_head(ml, t, arg)
  path = ENV['SCRIPT_NAME'] + "/#{ml}"
  h = ML::new(ml)
  if arg =~ /^(\d+)-(\d+)$/ then
    s = $1 == '' ? h.min : $1.to_i
    e = $2 == '' ? h.max : $2.to_i
  else
    s = 1
    e = $Step
  end

  if s <= h.min then
    prev_html = $PrevHTML
  else
    prev_s = s - $Step
    if prev_s < 1 then prev_s = 1 end
    prev_e = s - 1
    prev_html = "<a href=\"#{path}/#{t}/#{prev_s}-#{prev_e}\">#{$PrevHTML}</a>"
  end

  if e >= h.max then
    next_html = $NextHTML
  else
    next_s = e + 1
    next_e = next_s + $Step - 1
    next_html = "<a href=\"#{path}/#{t}/#{next_s}-#{next_e}\">#{$NextHTML}</a>"
  end

  if t == 'list' then
    a_html = "<a href=\"#{path}/thread/#{s}-#{e}\">#{$ThreadHTML}</a>"
  else
    a_html = "<a href=\"#{path}/list/#{s}-#{e}\">#{$ListHTML}</a>"
  end

  @buttons = "#{prev_html}#{next_html}#{a_html}<a href=\"#{path}/\">#{$UpHTML}</a>"

  puts <<EOS
Content-Type: text/html; charset=#{$Charset}

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=#{$Charset}">
<meta http-equiv="Content-Style-Type" content="text/css">
<title>#{ml}:#{s}-#{e}</title>
<link rel=stylesheet type="text/css" href="#{$CSS_URL}/w3ml.css">
</head>
<body>
<p class="button">#{@buttons}</p>
<h1>#{ml}:#{s}-#{e}</h1>
<pre class="list">
EOS

  return h, s, e
end

def put_tail()
  puts <<EOS
</pre>
<p class="button">#{@buttons}</p>
</body>
</html>
EOS
end
