require 'cgi'

def parse_date(s, h=0)
  if s == nil or s == '' then return nil end
  if s !~ /((?:\d\d)?\d\d)[\/-](\d?\d)[\/-](\d?\d)(?:\s+(\d?\d):(\d?\d))?/ then
    raise "#{s}: invalid format date string"
  end
  if $4 then h = 0 end
  t = Time::local($1, $2, $3, $4, $5) + h
end

def parse_string(s)
  if s == nil or s == '' then return nil end
  s.scan(/"(.+?)"|'(.+?)'|(\S+)/).flatten.compact
end

def search(ml, arg)
  cgi = CGI::new

  h = ML::new(ml)

  limit = 50
  offset = 0

  sdate = cgi.params['sdate'][0] if cgi.has_key? 'sdate'
  edate = cgi.params['edate'][0] if cgi.has_key? 'edate'
  subj = cgi.params['subject'][0] if cgi.has_key? 'subject'
  from = cgi.params['from'][0] if cgi.has_key? 'from'
  body = cgi.params['body'][0] if cgi.has_key? 'body'
  limit = cgi.params['limit'][0].to_i if cgi.has_key? 'limit'
  offset = cgi.params['offset'][0].to_i if cgi.has_key? 'offset'

  if cgi.params.length > 0 then
    n = h.count(parse_date(sdate, 0), parse_date(edate, 24*60*60-1), parse_string(subj), parse_string(from), parse_string(body))
    q = ENV['QUERY_STRING'].dup
    prev_html = $PrevHTML
    if offset > 0 then
      of = offset-limit >= 0 ? offset-limit : 0
      if q =~ /offset/ then
	q.sub!(/offset=\d+/, "offset=#{of}")
      else
	q << "&offset=#{of}"
      end
      prev_html = "<a href=\"search?#{q}\">#{$PrevHTML}</a>"
    end
    next_html = $NextHTML
    if offset+limit < n then
      of = offset+limit
      if q =~ /offset/ then
	q.sub!(/offset=\d+/, "offset=#{of}")
      else
	q << "&offset=#{of}"
      end
      next_html = "<a href=\"search?#{q}\">#{$NextHTML}</a>"
    end
    buttons = "#{prev_html}<a href=\"./\">#{$UpHTML}</a>#{next_html}"
  else
    buttons = "#{$PrevHTML}#{$UpHTML}#{$NextHTML}"
  end

  puts <<EOS
Content-Type: text/html; charset=#{$Charset}

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=#{$Charset}">
<meta http-equiv="Content-Style-Type" content="text/css">
<title>#{$mlname}</title>
<link rel=stylesheet type="text/css" href="#{$CSS_URL}/w3ml.css">
</head>
<body>

<p class="button">#{buttons}</p>

<h1>#{ml}: 検索</h1>

<form method="get" action="search">
<table>
<tr><th>本文<td><input name="body" type="text" size=60 value="#{body ? CGI.escapeHTML(body) : ''}">
<tr><th>Subject<td><input name="subject" type="text" size=60 value="#{subj ? CGI.escapeHTML(subj) : ''}">
<tr><th>From<td><input name="from" type="text" size=60 value="#{from ? CGI.escapeHTML(from) : ''}">
<tr><th>日付<td><input name="sdate" type="text" value="#{sdate}">〜<input name="edate" type="text" value="#{edate}">
<tr><th>表示数<td><select name="limit">
<option#{if limit <= 50 then ' selected' end}>50
<option#{if 50 < limit and limit <= 100 then ' selected' end}>100
<option#{if 100 < limit and limit <= 400 then ' selected' end}>400
</select>
</table>
<input type="submit" value="検索">
<input type="reset" value="リセット">
</form>

EOS

  if cgi.params.length > 0 then
    puts "<p>#{n}件見つかりました。#{offset+1}〜#{offset+limit}表示</p>"
    puts '<pre class="list">'
    h.search(parse_date(sdate,0), parse_date(edate, 24*60*60-1), parse_string(subj), parse_string(from), parse_string(body), limit, offset) do |n|
      printf "<a href=\"%s\">%s %5d %s [%-20.20s] %-40s</a>\n",
        "msg/#{n}",
        h[n].have_attach? ? $WithAttachHTML[0] : $WithAttachHTML[1],
        n,
        h[n].time.strftime('%Y-%m-%d %H:%M'),
        CGI.escapeHTML(h[n].from),
        CGI.escapeHTML(h[n].subject)
    end
    puts '</pre>'
  end

  puts <<EOS
<p class="button">#{buttons}</p>
</body>
</html>
EOS
end
