# $Id: compat.rb,v 1.3 2002/05/05 08:30:34 tommy Exp $

def compat(path_info)
  require 'cgi'
  unless path_info =~ /^\/(\w+)\.cgi(\?.*)?$/ then
    not_found
  end
  cginame = $1
  query = $2
  ENV['QUERY_STRING'] = query if query and query != ''
  cgi = CGI::new

  ml = cgi['ml'][0]
  if ml and File::exist? LIB_DIR+ml+'.conf' then
    load CONF_DIR+ml+'.conf'
  end
  require LIB_DIR+'w3ml'

  case cginame
  when 'index'
    if ml then
      require LIB_DIR+'top'
      top(ml)
    else
      require LIB_DIR+'list_ml'
      list_ml()
    end
  when 'list'
    min = cgi['min'][0]
    max = cgi['max'][0]
    require LIB_DIR+'list'
    list ml, "#{min}-#{max}"
  when 'thread'
    min = cgi['min'][0]
    max = cgi['max'][0]
    require LIB_DIR+'list'
    thread ml, "#{min}-#{max}"
  when 'msg'
    id = cgi['id'][0]
    require LIB_DIR+'msg'
    msg ml, id
  when 'raw'
    id = cgi['id'][0]
    require LIB_DIR+'raw'
    raw ml, id
  when 'search'
  else
    not_found
  end
end
