def hide_address(txt)
  return txt unless $HideAddress
  address_regex =
    /(("?)[-0-9a-zA-Z_.+?\/]+\2@[-0-9a-zA-Z]+\.[-0-9a-zA-Z.]+)/ # from quickml
  txt.gsub!(address_regex){|add| add.sub(/(@.).*/, '\1...')} # from quickml
  tel_regex = /(\d{2,4}-\d{2,4}-\d{4})/
  txt.gsub!(tel_regex){|tel| tel.gsub(/\d/, '?')}
  txt
end		
