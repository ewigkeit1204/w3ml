# $Id: w3ml-mysql.rb,v 1.6 2005/04/13 12:56:34 tommy Exp $

require 'mysql'

$my = Mysql::new(MysqlServer, MysqlUser, MysqlPass, MysqlDB)

def $my.quote(s)
  super(s.to_s)
end

def ml_list()
  ret = []
  $my.query('show tables like "%_header"').each do |t,|
    ret << t.sub(/_header/, '')
  end
  ret
end

class ML
  def initialize(ml)
    if ml !~ /^[a-z0-9_-]+$/i then
      not_found
    end
    @ml = ml.gsub(/[^a-z0-9_]/, '_')
    @table = @ml+'_header'
  end

  def table() @table end

  def min()
    if @min then return @min end
    @min = $my.query("select min(id) from #{@table}").fetch_row[0].to_i
  end

  def max()
    if @max then return @max end
    @max = $my.query("select max(id) from #{@table}").fetch_row[0].to_i
  end

  def range(s, e)
    rev = false
    if s > e then s, e = e, s; rev = true end
    $my.query("select id from #{@table} where id between #{s.to_i} and #{e.to_i} order by id").each do |n,|
      yield n.to_i
    end
  end

  def [](n)
    has?(n) ? Message::new(self, n) : nil
  end

  def has?(n)
    return $my.query("select 1 from #{@table} where id=#{n.to_i}").fetch_row
  end

  def ml()
    @ml
  end

  def count(sdate, edate, subj, from, body)
    s = make_sql('count(*)', sdate, edate, subj, from, body)
    $my.query(s).fetch_row[0].to_i
  end

  def search(sdate, edate, subj, from, body, limit, offset)
    s = make_sql("#{@table}.id", sdate, edate, subj, from, body)
    s << " order by id desc limit #{offset},#{limit}"
    $my.query(s).each do |r,|
      yield r.to_i
    end
  end

  def make_sql(items, sdate, edate, subj, from, body)
    table = @table
    where = []
    if sdate then where << "mdate >= #{sdate.to_i}" end
    if edate then where << "mdate <= #{edate.to_i}" end
    if subj then
      subj.each do |i|
	where << "subject like '%#{$my.quote i}%'"
      end
    end
    if from then
      from.each do |i|
	where << "mfrom like '%#{$my.quote i}%'"
      end
    end
    if body then
      table = table + ",#{@ml}_body"
      where << "#{@table}.id = #{@ml}_body.id"
      body.each do |i|
	where << "body like '%#{$my.quote i}%'"
      end
    end
    sql = "select #{items} from #{table}"
    if where.length > 0 then
      sql << ' where '+where.join(' and ')
    end
    sql
  end

  def lock()
    $my.query("lock tables #{@ml}_header write,#{@ml}_body write,#{@ml}_followed write,#{@ml}_attach write,#{@ml}_raw write,#{@ml}_msgid write")
  end

  def unlock()
    $my.query('unlock tables')
  end

  def add(id, h, b, a, m, r)
    $my.query("replace #{@ml}_header (id,mdate,mfrom,subject,reply_to) values (#{id},'#{h[0].tv_sec}','#{$my.quote h[1]}','#{$my.quote h[2]}','#{$my.quote h[3]}')")
    $my.query("replace #{@ml}_body (id,header,body) values (#{id},'#{$my.quote b[0]}','#{$my.quote b[1]}')")
    $my.query("replace #{@ml}_msgid (msgid,id) values ('#{$my.quote m}',#{id})")
    $my.query("replace #{@ml}_raw (id,message) values (#{id},'#{$my.quote r}')")
    if a then
      a.each do |path,fname,ct,body|
	$my.query("replace #{@ml}_attach (id,path,fname,ct,body) values (#{id},'#{$my.quote path}','#{$my.quote fname}','#{$my.quote ct}','#{$my.quote body}')")
      end
    end
    @min = @max = nil
  end

  def delete(id)
    $my.query("delete from #{@ml}_header where id=#{id}")
    $my.query("delete from #{@ml}_body where id=#{id}")
    $my.query("delete from #{@ml}_followed where id=#{id}")
    $my.query("delete from #{@ml}_raw where id=#{id}")
    $my.query("delete from #{@ml}_attach where id=#{id}")
    $my.query("delete from #{@ml}_msgid where id=#{id}")
    @min = @max = nil
  end

  def has_msgid?(m)
    $my.query("select 1 from #{@ml}_msgid where msgid='#{$my.quote m}'").num_rows > 0
  end

  def id_of_msgid(m)
    $my.query("select id from #{@ml}_msgid where msgid='#{$my.quote m}'").fetch_row[0].to_i
  end

end

class Message
  def initialize(ml, n)
    @ml = ml
    @n = n.to_i
  end

  def ml() @ml end
  def no() @n end

  def time() Time::at(msg_h[0]) end
  def from() msg_h[1] end
  def subject() msg_h[2] end
  def reply_to() msg_h[3] end

  def prev()
    $my.query("select max(id) from #{@ml.table} where id<#{@n}").fetch_row[0]
  end

  def next()
    $my.query("select min(id) from #{@ml.table} where id>#{@n}").fetch_row[0]
  end

  def have_attach?()
    $my.query("select 1 from #{attach_table} where id=#{@n}").fetch_row
  end

  def header()
    $my.query("select header from #{body_table} where id=#{@n}").fetch_row[0]
  end

  def body()
    $my.query("select body from #{body_table} where id=#{@n}").fetch_row[0]
  end

  def attach_list()
    if @attach_list then return @attach_list end
    @attach_list = []
    $my.query("select path,fname,ct from #{attach_table} where id=#{@n} order by path").each do |i|
      @attach_list << i
    end
    @attach_list
  end

  def attach(n)
    $my.query("select path,body from #{attach_table} where id=#{@n} order by path limit #{n.to_i},1").fetch_row[1]
  end

  def followed()
    r = $my.query("select c_list from #{followed_table} where id=#{@n}").fetch_row
    r ? r[0].split(',').map{|i| i.to_i} : []
  end

  def set_followed(f)
    $my.query("replace #{followed_table} set id='#{@n}', c_list='#{$my.quote f.uniq.sort.join(',')}'")
  end

  def raw()
    $my.query("select message from #{raw_table} where id=#{@n}").fetch_row[0]
  end

  def msg_h()
    if @msg_h then return @msg_h end
    r = $my.query("select mdate,mfrom,subject,reply_to from #{@ml.table} where id=#{@n}").fetch_row
    r[0] = r[0].to_i
    r[3] = r[3].to_i
    @msg_h = r
  end

  def body_table()
    "#{@ml.ml}_body"
  end

  def followed_table()
    "#{@ml.ml}_followed"
  end

  def attach_table()
    "#{@ml.ml}_attach"
  end

  def raw_table()
    "#{@ml.ml}_raw"
  end

end
