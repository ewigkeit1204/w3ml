# $Id: w3ml-gdbm.rb,v 1.5 2002/06/12 01:33:03 tommy Exp $

require 'gdbm'

def ml_list()
  ret = []
  Dir::foreach(DATA_DIR) do |f|
    next if f =~ /^\./
    if File::stat(DATA_DIR+'/'+f).ftype == 'directory' then
      ret << f
    end
  end
  ret
end

class ML
  @@cache = {}

  def ML::new(ml)
    return @@cache[ml] if @@cache.has_key? ml
    super ml
  end

  def initialize(ml)
    @ml = ml
  end

  def min()
    h_dbm.keys.map{|i| i.to_i}.min || 0
  end

  def max()
    h_dbm.keys.map{|i| i.to_i}.max || 0
  end

  def range(s, e)
    rev = false
    if s > e then s, e = e, s; rev = true end
    a = h_dbm.keys.map{|i| i.to_i}.delete_if{|i| i < s or i > e}.sort
    if rev then a.reverse! end
    a.each do |n|
      yield n
    end
    a
  end

  def [](n)
    Message::new(self, n)
  end

  def has?(n)
    h_dbm.has_key?(n.to_s)
  end

  def ml()
    @ml
  end

  def h_dbm()
    if defined? @@h_dbm then return @@h_dbm end
    @@h_dbm = GDBM::open(DATA_DIR+@ml+'/header.gdbm', 0644, GDBM::NOLOCK)
  end

  def b_dbm()
    if defined? @@b_dbm then return @@b_dbm end
    @@b_dbm = GDBM::open(DATA_DIR+@ml+'/body.gdbm', 0644, GDBM::NOLOCK)
  end

  def a_dbm()
    if defined? @@a_dbm then return @@a_dbm end
    @@a_dbm = GDBM::open(DATA_DIR+@ml+'/attach.gdbm', 0644, GDBM::NOLOCK)
  end

  def m_dbm()
    if defined? @@m_dbm then return @@m_dbm end
    @@m_dbm = GDBM::open(DATA_DIR+@ml+'/msgid.gdbm', 0644, GDBM::NOLOCK)
  end

  def f_dbm()
    if defined? @@f_dbm then return @@f_dbm end
    @@f_dbm = GDBM::open(DATA_DIR+@ml+'/followed.gdbm', 0644, GDBM::NOLOCK)
  end

  def r_dbm()
    if defined? @@r_dbm then return @@r_dbm end
    @@r_dbm = GDBM::open(DATA_DIR+@ml+'/raw.gdbm', 0644, GDBM::NOLOCK)
  end

  def count(sdate, edate, subj, from, body)
    r = do_search(sdate, edate, subj, from, body)
    r.length
  end

  def search(sdate, edate, subj, from, body, limit, offset)
    r = do_search(sdate, edate, subj, from, body)
    r[offset,limit].each do |n|
      yield n
    end
  end

  def do_search(sdate, edate, subj, from, body)
    if defined? @result and @result.has_key? [sdate,edate,subj,from,body] then
      return @result[[sdate,edate,subj,from,body]]
    end
    @result = {} unless defined? @result
    res = []
    c = 0
    o = 0
    if subj then subj.each do |i| i.downcase! end end
    if from then from.each do |i| i.downcase! end end
    if body then body.each do |i| i.downcase! end end
    range(max, min) do |n|
      m = self[n]
      if sdate and sdate > m.time then next end
      if edate and edate < m.time then next end
      if subj then
	f = true
	subj.each do |i|
	  if not m.subject.downcase.index(i) then f = false; break end
	end
	if not f then next end
      end
      if from then
	f = true
	from.each do |i|
	  if not m.from.downcase.index(i) then f = false; break end
	end
	if not f then next end
      end
      if body then
	f = true
	body.each do |i|
	  if not m.body.downcase.index(i) then f = false; break end
	end
	if not f then next end
      end
      res << n
    end
    @result[[sdate,edate,subj,from,body]] = res
  end

  def lock()
    @lockfile = DATA_DIR+@ml+'/lock'
    @lockf = File::open(@lockfile, 'w')
    @lockf.flock File::LOCK_EX
  end

  def unlock()
    @lockf.flock File::LOCK_UN
    @lockf.close
  end

  def add(id, h, b, a, m, r)
    n = id.to_s
    h_dbm[n] = Marshal::dump(h)
    b_dbm[n] = Marshal::dump(b)
    m_dbm[m] = Marshal::dump(id) if m
    r_dbm[n] = Marshal::dump(r) if r
    a_dbm				# for creating
    if a then
      a_dbm[n] = Marshal::dump(a.map{|x| x[0,3]})
      a.each do |x|
	a_dbm[n+'.'+x[0]] = x[3]
      end
    end
  end

  def delete(id)
    n = id.to_s
    if h_dbm.has_key? n then
      pa = h_dbm[n][3].to_s
    end
    h_dbm.delete n
    b_dbm.delete n
    f_dbm.delete n
    r_dbm.delete n
    if a_dbm.has_key? n then
      Marshal::load(a_dbm[n]).each do |m,|
	a_dbm.delete "#{n}.#{m}"
      end
      a_dbm.delete n
    end
    if pa and f_dbm.has_key? pa then
      f = Marshal::load(f_dbm[pa])
      f.delete n.to_i
      f_dbm[n] = Marshal::dump(f)
    end
    m_dbm.delete_if do |m,i| Marshal::load(i) == id end
  end

  def has_msgid?(m)
    m_dbm.has_key?(m)
  end

  def id_of_msgid(m)
    Marshal::load m_dbm[m]
  end

end

class Message
  def initialize(ml, n)
    @ml = ml
    @n = n
  end

  def ml() @ml end
  def no() @n end
  def time() msg_h[0] end
  def from() msg_h[1] end
  def subject() msg_h[2] end
  def reply_to() msg_h[3] end

  def prev()
    @ml.h_dbm.keys.map{|i| i.to_i}.delete_if{|i| i>=@n}.max
  end

  def next()
    @ml.h_dbm.keys.map{|i| i.to_i}.delete_if{|i| i<=@n}.min
  end

  def have_attach?()
    a_dbm.has_key? @n.to_s
  end

  def header()
    message[0]
  end

  def body()
    message[1]
  end

  def attach_list()
    if @attach_list then return @attach_list end
    @attach_list = a_dbm.has_key?(@n.to_s) ? Marshal::load(a_dbm[@n.to_s]) : []
  end

  def attach(n)
    a_dbm.has_key?("#{@n}.#{n}") ? a_dbm["#{@n}.#{n}"] : nil
  end

  def message()
    if @message then return @message end
    @message = Marshal::load(b_dbm[@n.to_s])
  end

  def followed()
    if @follwed then return @followed end
    @followed = f_dbm.has_key?(@n.to_s) ? Marshal::load(f_dbm[@n.to_s]) : []
  end

  def set_followed(f)
    f_dbm[@n.to_s] = Marshal::dump(f.uniq.sort)
  end

  def raw()
    if @raw then return @raw end
    @raw = Marshal::load(r_dbm[@n.to_s])
  end

  def msg_h()
    if @msg_h then return @msg_h end
    @msg_h = Marshal::load(@ml.h_dbm[@n.to_s])
  end

  def b_dbm()
    @ml.b_dbm
  end

  def a_dbm()
    @ml.a_dbm
  end

  def f_dbm()
    @ml.f_dbm
  end

  def r_dbm()
    @ml.r_dbm
  end

end
