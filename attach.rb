# $Id: attach.rb,v 1.1.1.1 2002/04/14 17:18:23 tommy Exp $

def attach(ml, n, arg)
  n = n.to_i
  if n <= 0 then
    not_found
  end
  a_no = arg.to_i

  m = ML::new(ml)[n]
  if not m.attach_list[a_no] then
    not_found
  end
  ct = m.attach_list[a_no][2]
  body = m.attach(a_no)

  puts <<EOS
Content-Type: #{ct}
Content-Length: #{body.length}

EOS
  print body
end
