%define snapshot .git20160420
%define gitsha .7b1e7f4
%define release_version 11

Name:           w3ml
Version:        0.4
Release:        %{release_version}%{snapshot}%{gitsha}%{?dist}
Summary:        A mailing list log viewer
License:        Unknown
URL:            https://bitbucket.org/ewigkeit1204/w3ml/
Source0:        %{name}-%{version}-%{release_version}%{snapshot}%{gitsha}.tar.xz

BuildRequires:  ruby
Requires:       ruby httpd

BuildArch:      noarch

%description
A mailing list log viewer

%prep
%setup -q


%build
LANG=ja_JP.UTF-8 ruby ./configure --batch \
  --dest-dir=$RPM_BUILD_ROOT \
  --ruby-path=%{_bindir}/ruby \
  --bin-dir=%{_bindir} \
  --lib-dir=%{_datadir}/w3ml \
  --conf-dir=%{_sysconfdir}/w3ml \
  --data-dir=%{_sharedstatedir}/w3ml \
  --cgi-dir=%{_localstatedir}/www/cgi-bin \
  --css-dir=%{_localstatedir}/www/html/w3ml/css \
  --css-url=/w3ml/css


%install
rm -rf $RPM_BUILD_ROOT
ruby ./install.rb
mv $RPM_BUILD_ROOT%{_bindir}/entry $RPM_BUILD_ROOT%{_bindir}/w3ml-entry
mv $RPM_BUILD_ROOT%{_bindir}/setup $RPM_BUILD_ROOT%{_bindir}/w3ml-setup


%files
%defattr(-,root,root)
%doc README.ja
%{_bindir}/*
%{_datadir}/w3ml/*
%config(noreplace) %{_sysconfdir}/w3ml/*
%dir %{_sharedstatedir}/w3ml
%{_localstatedir}/www/cgi-bin/*
%{_localstatedir}/www/html/w3ml/css/*


%changelog
* Wed Apr 20 2016 Keisuke Kamada <ewigkeit1204@gmail.com> - 0.4-11.git20160420.7b1e7f4
- Remove cached value, ml.min and ml.max.

* Tue Apr 19 2016 Keisuke Kamada <ewigkeit1204@gmail.com> - 0.4-10.git20160419.7c70ba4
- Fix invalid byte sequence error.

* Tue Apr 19 2016 Keisuke Kamada <ewigkeit1204@gmail.com> - 0.4-9.git20160419.bae9bac
- Fix content-type parse error.

* Tue Apr 19 2016 Keisuke Kamada <ewigkeit1204@gmail.com> - 0.4-8.git20160419.510714a
- Add a new parameter $AlwaysAppend.
- Fix configure script.

* Thu Oct 29 2015 Keisuke Kamada <ewigkeit1204@gmail.com> - 0.4-7.git20151029.270e6b9
- Use CGI.escapeHTML instead of self implementation.
- Use CGI.escape instead of self implementation.

* Thu Oct 28 2015 Keisuke Kamada <ewigkeit1204@gmail.com> - 0.4-6.git20151028.d649989
- File::LOCK_UN instead of removing lock file.

* Thu Oct 27 2015 Keisuke Kamada <ewigkeit1204@gmail.com> - 0.4-5.git20151027.95bf39c
- Specify GDBM::NOLOCK
- Fix deleting array elements.

* Thu Oct 27 2015 Keisuke Kamada <ewigkeit1204@gmail.com> - 0.4-4.git20151027.a5d66f4
- Prevent TypeError

* Thu Oct 27 2015 Keisuke Kamada <ewigkeit1204@gmail.com> - 0.4-3.git20151027.32259b9
- Fix spec file

* Thu Oct 27 2015 Keisuke Kamada <ewigkeit1204@gmail.com> - 0.4-2.git20151027.32259b9
- Default as utf-8
- Sort ml list

* Thu Oct 15 2015 Keisuke Kamada <ewigkeit1204@gmail.com> - 0.4-1.git20151015.9399f97
- initial build

