def not_found()
  puts <<EOS
Status: 404 Not Found
Content-Type: text/html

<h1>Not Found</h1>
EOS
  exit
end

def require_slash()
  puts "Status: 301 Moved Permanently\n"
  puts "Location: #{ENV['REQUEST_URI']}/\n\n"
  exit
end

require LIB_DIR+'w3ml-'+$Database
