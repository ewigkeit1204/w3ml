# -*- ruby -*-
# $Id: umail.rb,v 1.2 2002/05/05 17:34:57 tommy Exp $

require 'time'
require 'nkf'

class UMail

  @@strictly = false

  def UMail.strictly=(flag)
    @@strictly = flag
  end

  def UMail.strictly()
    @@strictly
  end

  class Base64
    # Bエンコードされたヘッダ文字列をデコードする
    def Base64.hdecode(str)
      str.unpack('m')[0]
    end

    # Base64 デコード
    def Base64.decode(str)
      Base64.hdecode str
    end
  end

  class QuotedPrintable
    # Qエンコードされたヘッダ文字列をデコードする
    def QuotedPrintable.hdecode(str)
      str.gsub('_', ' ').gsub(/=([0-9A-F][0-9A-F])/o) do $1.hex.chr end
    end

    # QuotedPrintable デコード
    def QuotedPrintable.decode(str)
      str.gsub(/[ \t]+$/o, '').gsub(/=\r?\n/o, '').
	gsub(/=([0-9A-F][0-9A-F])/o) do $1.hex.chr end
    end
  end

  NkfFlag = {'iso-2022-jp' => 'J', 'euc-jp' => 'E', 'shift_jis' => 'S', 'utf-8' => 'W'}

  def UMail.mdecode_token(s)
    if s !~ /\A=\?([a-z0-9_-]+)\?(Q|B)\?([^?]+)\?=\Z/io then
      s
    else
      charset, encoding, text = $1, $2, $3
      c = NkfFlag[charset.downcase]
      if encoding.downcase == 'q' then s = QuotedPrintable.hdecode(text)
      else s = Base64.hdecode(text)
      end
      NKF::nkf("-w#{c}m0", s)
    end
  end

  class ParseError < StandardError
  end

  class HeaderField
    def initialize(n)
      @name = n
      @raw_body = []
    end

    def add(s)
      @raw_body << s
    end

    def to_s()
      @raw_body.join("\n")
    end

    def body()
      parse if not @parsed
      @parsed_body
    end

    def parse()
      @parsed_body = @raw_body.join("\n")
      @parsed = true
    end

    def strictly?()
      @strictly == nil ? UMail::strictly : @strictly
    end

    def mdecode(list)
      if not strictly? then
	list.join.gsub(/\?=\s+=\?/, '?==?').gsub(/=\?[a-z0-9_-]+\?(Q|B)\?[^?]+\?=/io) do UMail.mdecode_token($&) end
      else
	cs = ''
	sp = ''
	after_encode = false
	list.each do |c|
	  if c =~ /\A\s+\Z/ then c = ' ' end
	  if after_encode and c == ' ' then sp = c; next end
	  c2 = UMail.mdecode_token c
	  if after_encode and c != c2 then cs << c2; next end
	  after_encode = c != c2
	  cs << sp + c2
	  sp = ''
	end
	cs
      end
    end

  end

  class StringH < HeaderField
    def parse()
      @parsed_body = mdecode(@raw_body.join(' ').split(/(\s+)/o))
      @parsed = true
    end
  end

  class StructH < HeaderField

    WDay = ['Mon','Tue','Wed','Thu','Fri','Sat','Sun']
    Mon = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
    ZoneStricted = {
      'UT' => 0, 'GMT' => 0,
      'EST' => -5, 'EDT' => -4,
      'CST' => -6, 'CDT' => -5,
      'MST' => -7, 'MDT' => -6,
      'PST' => -8, 'PDT' => -7,
      'A' => 1, 'B' => 2, 'C' => 3, 'D' => 4, 'E' => 5, 'F' => 6,
      'G' => 7, 'H' => 8, 'I' => 9, 'K' => 10, 'L' => 11, 'M' => 12,
      'N' => -1, 'O' => -2, 'P' => -3, 'Q' => -4, 'R' => -5, 'S' => -6,
      'T' => -7, 'U' => -8, 'V' => -9, 'W' => -10, 'X' => -11, 'Y' => -12,
      'Z'=>0, 
    }

    Zone = ZoneStricted.dup
    Zone['JST'] = 9
    
    Specials = "()<>@,;:\\\".[]".split(//)
    TokenDelimiterRe = /[\[\]()<>@,;:\\.]|\s+|\"(?:[^\\\"]|\\.)*\"/

    def parse()
      @tokens = split_array_token(@raw_body)
      @parsed_body = @tokens.join
      if @comments then @parsed_body << ' ' + @comments.join(' ') end
      @parsed = true
    end

    def comments()
      parse if not @parsed
      @comments
    end

    private

    # 配列中の文字列をトークンに分割する
    def split_array_token(s)
      ret = []
      s.each do |t|
	ret.concat split_token(t)
      end
      ret
    end

    # 配列 ary を要素 d で分割する
    def array_split(ary, d, &p)
      a = []
      ret = [a]
      ary.each do |i|
	if i != d then
	  a << i
	else
	  a = []
	  ret << a
	end
      end
      if p then
	ret.each do |i| yield i end
	return nil
      end
      ret
    end

    # 文字列 s をトークンに分割する
    def split_token(s, re=TokenDelimiterRe)
      comments = []
      s0 = s
      ret = []
      while s =~ re
	ret << $` if $` != ''
	m, s = $&, $'		#'
	case m[0]
	when ?(
	  cnt = 1
	  com = [m]
	  while cnt > 0
	    if s == '' then raise ParseError::new(s0) end
	    if s =~ /\A\s+|\A([^()\\\s]|\\.)*/o then
	      com << $&
	      s = $'		#'
	    end
	    case s[0]
	    when ?)
	      com << ')'
	      s[0] = ''
	      cnt -= 1
	    when ?(
	      com << '('
	      s[0] = ''
	      cnt += 1
	    end
	  end
	  com.delete ''
	  comments << mdecode(com)
	when 0x20, 0x09
	  ret << ' ' if ret[-1] != ' '
	else
	  ret << m
	end
      end
      ret << s
      ret.delete ''
      if ret[0] == ' ' then ret.delete_at(0) end
      if ret[-1] == ' ' then ret.delete_at(-1) end
      if comments.length > 0 then @comments = comments end
      ret
    end
  end

  class DateH < StructH
    def parse()
      @tokens = split_token(@raw_body[0])
      @date = Time.rfc2822(@raw_body[0])
      @parsed_body = @tokens.join
      if @comments then @parsed_body << ' ' + @comments.join(' ') end
      @parsed = true
    end
    def date()
      parse if not @parsed
      @date
    end
  end

  class RecvH < StructH
  end

  class RetpathH < StructH
  end

  class SaddrH < StructH
  end

  class MailAddr
    def initialize(addr, phrase)
      @addr = addr
      @phrase = phrase
    end
    attr_reader :addr, :phrase
    def to_s()
      @phrase ? "#{@phrase} <#{@addr}>" : "<#{@addr}>"
    end
    def inspect()
      "#<UMail::MailAddr: #{@addr}>"
    end
  end

  class MaddrH < StructH
    def parse()
      @tokens = []
      @addrs = []
      @raw_body.each do |b|
	tokens = split_token(b)
	@tokens.concat @tokens
	array_split(tokens, ',') do |a|
	  i = a.index '<'
	  if i then
	    phrase = a[0 .. i-1] if i > 0
	    addr = a[i+1 .. -1]
	    addr.delete ' '
	    if addr[-1] != '>' then raise ParseError::new("invalid '<'") end
	    addr.delete_at(-1)
	  else
	    phrase = nil
	    addr = a
	  end
	  if phrase then
	    if phrase[0] == ' ' then phrase.delete_at(0) end
	    if phrase[-1] == ' ' then phrase.delete_at(-1) end
	    if strictly? then
	      phrase.each do |p|
		if Specials.include? p then
		  raise ParseError::new("invalid phrase (#{p})")
		end
	      end
	    end
	    phrase = mdecode phrase
	  end
	  addr.delete ' '
	  if strictly? then
	    if not addr.include? '@' then
	      raise ParseError::new("'@' not in address")
	    end
	  end
	  @addrs << MailAddr::new(addr.join, phrase)
	end
      end
      @parsed_body = @addrs.collect{|a| a.to_s}.join(', ')
      if @comments then @parsed_body << ' ' + @comments.join(' ') end
      @parsed = true
    end

    def addrs()
      parse if not @parsed
      @addrs
    end

  end

  class MsgidH < StructH
    def parse()
      @tokens = split_array_token @raw_body
      @tokens.delete ' '
      if @tokens[0] != '<' or @tokens[-1] != '>' then raise ParseError end
      @parsed_body = @tokens.join
      if @comments then @parsed_body << ' ' + @comments.join(' ') end
      @msgid = @tokens.join
      @parsed = true
    end
    def msgid()
      parse if not @parsed
      @msgid
    end
  end

  class RefH < StructH
    def parse()
      super
      @msgids = []
      @phrases = ['']
      in_msgid = nil
      @tokens.each_index do |i|
	case @tokens[i]
	when '<'
	  in_msgid = i
	  @phrases << ''
	when '>'
	  if not in_msgid then raise ParseError end
	  id = @tokens[in_msgid..i]
	  id.delete ' '
	  @msgids << id.join
	  in_msgid = nil
	else
	  if not in_msgid then
	    @phrases[-1] << @tokens[i]
	  end
	end
      end
      @phrases.each do |i| i.gsub!(/^\s+|\s+$/, '') end
      @phrases.delete ''
    end
    def msgids()
      parse if not @parsed
      @msgids
    end
    def phrases()
      parse if not @parsed
      @phrases
    end
  end

  class KeyH < StructH
  end

  class EncH < StructH
  end

  class VersionH < StructH
  end

  class CTypeH < StructH
    TokenDelimiterRe = /[\[\]()<>@,;:\\\/=.]|\s+|\"(?:[^\\\"]|\\.)*\"/
    def split_token(s)
      super(s, TokenDelimiterRe)
    end
    def parse()
      @tokens = split_token(@raw_body[0])
      @parsed_body = @tokens.join
      tokens = @tokens.map(&:strip).reject(&:empty?)
      @maintype = tokens[0].downcase
      @subtype = nil
      @params = {}
      if tokens.length > 1 then
	if tokens[1] != '/' then raise ParseError end
	@subtype = tokens[2].downcase
	tokens = tokens[3 .. tokens.size]
	while tokens.length > 0
	  if tokens[0] != ';' or tokens[2] != '=' then
	    raise ParseError if strictly?
	    break
	  end
	  pn, pv = tokens[1], tokens[3]
	  @params[pn.downcase] = pv.gsub(/^\"|\"$/, '')
	  tokens = tokens[4 .. tokens.size]
	end
      end
      @parsed = true
    end
    def maintype()
      parse if not @parsed
      @maintype
    end
    def subtype()
      parse if not @parsed
      @subtype
    end
    def params()
      parse if not @parsed
      @params
    end
  end

  class CEncodingH < StructH
    def encoding()
      parse if not @parsed
      @tokens[0].downcase
    end
  end

  class CDispositionH < StructH
    TokenDelimiterRe = /[\[\]()<>@,;:\\\/=.]|\s+|\"(?:[^\\\"]|\\.)*\"/
    def split_token(s)
      super(s, TokenDelimiterRe)
    end
    def parse()
      @tokens = split_token(@raw_body[0])
      @parsed_body = @tokens.join
      tokens = @tokens.dup
      tokens.delete ' '
      @disposition = tokens[0].downcase
      tokens.delete_at(0)
      @params = {}
      while tokens.length > 0
	if tokens[0] != ';' or tokens[2] != '=' then
	  raise ParseError if strictly?
	  break
	end
	if tokens[0] != ';' or tokens[2] != '=' then break end
	pn, pv = tokens[1], tokens[3]
	@params[pn.downcase] = pv.gsub(/^\"|\"$/, '')
	tokens = tokens[4 .. tokens.size]
      end
      @parsed = true
    end
    def disposition()
      parse if not @parsed
      @disposition
    end
    def params()
      parse if not @parsed
      @params
    end
  end

  HeaderClass = {
    'date'                      => DateH,
    'resent-date'               => DateH,
    'received'                  => RecvH,
    'return-path'               => RetpathH,
    'sender'                    => SaddrH,
    'resent-sender'             => SaddrH,
    'to'                        => MaddrH,
    'cc'                        => MaddrH,
    'bcc'                       => MaddrH,
    'from'                      => MaddrH,
    'reply-to'                  => MaddrH,
    'resent-to'                 => MaddrH,
    'resent-cc'                 => MaddrH,
    'resent-bcc'                => MaddrH,
    'resent-from'               => MaddrH,
    'resent-reply-to'           => MaddrH,
    'message-id'                => MsgidH,
    'resent-message-id'         => MsgidH,
    'content-id'                => MsgidH,
    'in-reply-to'               => RefH,
    'references'                => RefH,
    'keywords'                  => KeyH,
    'encrypted'                 => EncH,
    'mime-version'              => VersionH,
    'content-type'              => CTypeH,
    'content-transfer-encoding' => CEncodingH,
    'content-disposition'       => CDispositionH,
    'subject'                   => StringH,
    'comments'                  => StringH,
    'content-description'       => StringH
  }

  def initialize(str)
    @header = {}
    raw = str.encode("UTF-16BE", "UTF-8", :invalid => :replace, :undef => :replace, :replace => '?').encode("UTF-8")
    @header_s, @body_s = raw.gsub(/\r\n/o, "\n").split(/\n\n/o, 2)
    @header_s.gsub(/\n[ \t]+/o, " ").split(/\n/o).each do |s|
      hname, hbody = s.split(/:\s*/o, 2)
      hn = hname.downcase
      c = HeaderClass.has_key?(hn) ? HeaderClass[hn] : HeaderField
      @header[hn] = c::new(hname) if not @header.has_key? hn
      @header[hn].add hbody
    end
  end

  attr :header

  def [](n)
    @header[n.downcase]
  end

  def each()
    @header.each do |a|
      yield a
    end
  end

  def has_key?(k)
    @header.has_key?(k)
  end

  def from()
    if @header.has_key? 'from' and @header['from'].addrs.length > 0 then
      @header['from'].addrs[0].addr
    else
      nil
    end
  end

  def from_phrase()
    if @header.has_key? 'from' and @header['from'].addrs.length > 0 then
      @header['from'].addrs[0].phrase
    else
      nil
    end
  end

  def subject()
    if @header.has_key? 'subject' then
      @header['subject'].body
    else
      nil
    end
  end

  def maintype()
    if @header.has_key? 'content-type' then
      @header['content-type'].maintype
    else
      nil
    end
  end

  def subtype()
    if @header.has_key? 'content-type' then
      @header['content-type'].subtype
    else
      nil
    end
  end

  def multipart?()
    @header.has_key? 'content-type' and @header['content-type'].maintype == 'multipart'
  end

  def parse_body()
    if not multipart? then
      @body = @body_s
      if @header.has_key? 'content-transfer-encoding' then
	case @header['content-transfer-encoding'].encoding
	when 'base64'
	  @body = Base64::decode(@body)
	when 'quoted-printable'
	  @body = QuotedPrintable::decode(@body)
	end
      end
      if @header.has_key? 'content-type' then
	if @header['content-type'].maintype == 'text' then
	  if @header['content-type'].params.has_key? 'charset' then
	    case @header['content-type'].params['charset'].gsub(/^\"|\"$/, '')
	    when /^iso-2022-jp$/i
	      @body = NKF::nkf('-Jwm0', @body)
	    when /^shift_jis$/i
	      @body = NKF::nkf('-Swm0', @body)
	    when /^euc-jp$/i
	      @body = NKF::nkf('-Ewm0', @body)
	    else
	      @body = NKF::nkf('-wm0', @body)
	    end
	  else
	    @body = NKF::nkf('-Jwm0', @body)
	  end
	end
      else
	@body = NKF::nkf('-Jwm0', @body)
      end
      @preamble = @body
      @epilogue = ""
    else
      b = @header['content-type'].params['boundary']
      if b == nil then raise ParseError::new("no boundary in content-type") end
      bre = Regexp::new '^'+Regexp::quote("--#{b}")+'[ \t]*\n'
      cre = Regexp::new '^'+Regexp::quote("--#{b}--")+'[ \t]*(\n|\Z)'
      a = @body_s.split(bre)
      if a[-1] =~ cre then
	a[-1] = $`
	epi = $'
      else
	raise ParseError::new('no close-delimiter')
      end
      pre = a.shift
      @body = @preamble = pre
      @epilogue = epi
      @parts_a = a.collect do |m| UMail::new m end
    end
    @body_parsed = true
  end

  def body()
    parse_body if not @body_parsed
    @body
  end

  alias preamble body

  def epilogue()
    parse_body if not @body_parsed
    @epilogue
  end

  def parts()
    parse_body if not @body_parsed
    @parts_a
  end

  attr :strictly, true

end
