# $Id: raw.rb,v 1.1.1.1 2002/04/14 17:18:23 tommy Exp $

def raw(ml, arg)
  n = arg.to_i
  m = ML::new(ml)[n]
  if m == nil then not_found end
  puts "Content-Type: message/rfc822\n\n"
  print m.raw
end
