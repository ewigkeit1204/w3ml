$:.unshift( File::dirname( __FILE__ ) )
require 'cgi'
require 'util'

def print_tree(path, m, del=nil, indent='', subj='', cur=nil)
  nsubj = m.subject.gsub(/\s+/, '').sub(/^(Re:)+/i, '')
  if cur then
    if m.no == cur then
      print "<span class=\"current\">-&gt;&nbsp;"
    else
      print "<span>&nbsp;&nbsp;&nbsp;"
    end
  end
  indent_s = indent.split(//).map{|i|$TreeCharacter[i.to_i]}.join
  printf "<a href=\"%s\">%s %5d %s %s[%-20.20s] %-#{40-indent_s.length}s</a>",
    "#{path}/msg/#{m.no}",
    m.have_attach? ? $WithAttachHTML[0] : $WithAttachHTML[1],
    m.no,
    m.time.strftime('%Y-%m-%d %H:%M'),
    indent_s,
    CGI.escapeHTML(hide_address(m.from)),
    if subj == nsubj then '' else CGI.escapeHTML(m.subject) end
  puts "</span>"
  del[m.no] = true if del
  indent.tr!('01', '32')
  m.followed.each_index do |i|
    if i == m.followed.length-1 then ii = '0' else ii = '1' end
    print_tree(path, Message::new(m.ml, m.followed[i]), del, indent+ii, nsubj, cur)
  end
end
